


### Notes

to check if a minecraft server is up:
```
printf '\xfe\x01' | nc localhost 25565
```

for now this is required to run the server
```
kubectl port-forward -n minecraft deployment/velocity 25565:25565 --address 0.0.0.0
```

```
microk8s kubectl run -n minecraft -it --rm --restart=Never busybox --image=gcr.io/google-containers/busybox printf '\xfe\x01' | nc lobby-service.minecraft.svc.cluster.local 25565
```
