#!/bin/bash -e

ls -la /root
echo 
ls -la /velocity-config
echo
ls -la /velocity-secret
cat  /root/velocity.toml

cd /root
java -Xms200M -Xmx200M -XX:+UseG1GC -XX:G1HeapRegionSize=4M -XX:+UnlockExperimentalVMOptions -XX:+ParallelRefProcEnabled -XX:+AlwaysPreTouch -XX:MaxInlineLevel=15 -jar /root/velocity.jar

