


to start the dashboard:
```
minikube dashboard --url --port 10433

microk8s kubectl port-forward -n kube-system service/kubernetes-dashboard 10443:443
```

to expose everything to the entire network
```
minikube kubectl proxy -- --address 0.0.0.0 --accept-hosts 10.0.0.10
```


https://sir5kong.github.io/helm-charts-hub/bitnami/redis/
redis splash:
```

** Please be patient while the chart is being deployed **

Redis&reg; can be accessed on the following DNS names from within your cluster:

    velocity-redis-master.minecraft.svc.cluster.local for read/write operations (port 6379)
    velocity-redis-replicas.minecraft.svc.cluster.local for read-only operations (port 6379)



To get your password run:

    export REDIS_PASSWORD=$(kubectl get secret --namespace minecraft velocity-redis -o jsonpath="{.data.redis-password}" | base64 -d)

To connect to your Redis&reg; server:

1. Run a Redis&reg; pod that you can use as a client:

   kubectl run --namespace minecraft redis-client --restart='Never'  --env REDIS_PASSWORD=$REDIS_PASSWORD  --image docker.io/bitnami/redis:7.2.4-debian-12-r12 --command -- sleep infinity

   Use the following command to attach to the pod:

   kubectl exec --tty -i redis-client \
   --namespace minecraft -- bash

2. Connect using the Redis&reg; CLI:
   REDISCLI_AUTH="$REDIS_PASSWORD" redis-cli -h velocity-redis-master
   REDISCLI_AUTH="$REDIS_PASSWORD" redis-cli -h velocity-redis-replicas

To connect to your database from outside the cluster execute the following commands:

    kubectl port-forward --namespace minecraft svc/velocity-redis-master 6379:6379 &
    REDISCLI_AUTH="$REDIS_PASSWORD" redis-cli -h 127.0.0.1 -p 6379

WARNING: There are "resources" sections in the chart not set. Using "resourcesPreset" is not recommended for production. For production installations, please set the following values according to your workload needs:
  - master.resources
  - replica.resources
+info https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/
```

helm install -n minecraft mc-ingess qumine/ingress-controller
